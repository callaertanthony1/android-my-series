package com.rizomm.myseries.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rizomm.myseries.ui.fragment.ShowDetailFragment;
import com.rizomm.myseries.ui.fragment.ShowListFragment;
import com.rizomm.myseries.ui.fragment.TabFragment;

/**
 * Created by anthonycallaert on 10/03/16.
 */
public class GlobalPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public GlobalPagerAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabFragment tab1 = new TabFragment();
                return tab1;
            case 1:
                ShowDetailFragment tab2 = new ShowDetailFragment();
                return tab2;
            case 2:
                TabFragment tab3 = new TabFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
