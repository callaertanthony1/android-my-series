package com.rizomm.myseries.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rizomm.myseries.model.Show;

import java.util.List;

/**
 * Created by anthonycallaert on 10/03/16.
 */
public class ShowPagerAdapter extends FragmentStatePagerAdapter {

    private List<Show> showList;

    public ShowPagerAdapter(FragmentManager fm, List<Show> showList) {
        super(fm);
        this.showList = showList;
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}
