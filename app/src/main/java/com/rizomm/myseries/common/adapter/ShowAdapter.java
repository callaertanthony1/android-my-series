package com.rizomm.myseries.common.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rizomm.myseries.R;
import com.rizomm.myseries.model.Show;

import java.util.List;

/**
 * Created by anthonycallaert on 03/03/16.
 */
public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ViewHolder> {
    private List<Show> mShowList;

    public ShowAdapter(List<Show> mShowList) {
        this.mShowList = mShowList;
    }

    @Override
    public ShowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.serie_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowAdapter.ViewHolder holder, int position) {
        Show show = mShowList.get(position);
        holder.mNameTxv.setText(show.getName());
        holder.mSummaryTxv.setText(show.getPreview());
    }

    @Override
    public int getItemCount() {
        return this.mShowList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mNameTxv;
        private TextView mSummaryTxv;

        public ViewHolder(View itemView) {
            super(itemView);
            mNameTxv = (TextView) itemView.findViewById(R.id.show_name);
            mSummaryTxv = (TextView) itemView.findViewById(R.id.show_summary);
        }
    }
}
