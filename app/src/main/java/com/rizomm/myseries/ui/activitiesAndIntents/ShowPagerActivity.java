package com.rizomm.myseries.ui.activitiesAndIntents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.rizomm.myseries.common.adapter.ShowPagerAdapter;
import com.rizomm.myseries.model.Show;

import java.util.List;

/**
 * Created by anthonycallaert on 10/03/16.
 */
public class ShowPagerActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private ShowPagerAdapter showPagerAdapter;
    private List<Show> showList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
