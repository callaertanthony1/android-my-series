package com.rizomm.myseries.model;

import java.io.Serializable;

/**
 * Created by anthonycallaert on 03/03/16.
 */
public class Show implements Serializable {
    private int id;
    private String url;
    private String name;
    private String language;
    private String summary;

    public Show(int id, String url, String name, String language, String summary) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.language = language;
        this.summary = summary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getPreview(){
        if(this.summary.length() > 100)
            return this.summary.substring(0, 96) + "...";
        else
            return this.summary;
    }
}
