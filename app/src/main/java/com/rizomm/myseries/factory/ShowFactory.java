package com.rizomm.myseries.factory;

import com.rizomm.myseries.model.Show;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anthonycallaert on 03/03/16.
 */
public class ShowFactory {

    public static List<Show> getShowList(){
        List<Show> showList = new ArrayList<>();

        showList.add(new Show(1, "http://www.tvmaze.com/shows/1/under-the-dome", "Under the Dome", "English", "Under the Dome is the story of a small town that is suddenly and inexplicably sealed off from the rest of the world by an enormous transparent dome. The town's inhabitants must deal with surviving the post-apocalyptic conditions while searching for answers about the dome, where it came from and if and when it will go away."));
        showList.add(new Show(2, "http://www.tvmaze.com/shows/2/person-of-interest", "Person of Interest", "English", "You are being watched. The government has a secret system, a machine that spies on you every hour of every day. I know because I built it. I designed the Machine to detect acts of terror but it sees everything. Violent crimes involving ordinary people. People like you. Crimes the government considered \\\"irrelevant.\\\" They wouldn't act so I decided I would. But I needed a partner. Someone with the skills to intervene. Hunted by the authorities, we work in secret. You'll never find us. But victim or perpetrator, if your number is up, we'll find you."));

        return showList;
    }
}
